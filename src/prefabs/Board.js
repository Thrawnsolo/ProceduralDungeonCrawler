import Phaser from 'phaser';
import Item from './Item';
import Enemy from './Enemy';

const Board = function (state, data) {
    this.state = state;
    this.levelData = data.levelData;
    this.coefs = this.levelData.coefs;
    this.game = state.game;
    this.rows = data.rows;
    this.cols = data.cols;
    this.numCells = this.rows * this.cols;
    this.mapElements = state.mapElements;
    this.darknessTiles = state.darknessTiles;
    this.tileSize = data.tileSize;
    for (let i = 0; i < this.rows; i += 1) {
        for (let j = 0; j < this.cols; j += 1) {
            const tile = new Phaser.Sprite(
                this.game,
                j * this.tileSize,
                i * this.tileSize,
                'rockLand'
            );
            tile.row = i;
            tile.col = j;
            this.state.backgroundTiles.add(tile);

            tile.inputEnabled = true;
            tile.events.onInputDown.add((clickedTile) => {
                const darkTile = this.darknessTiles.children.find(darknessTile => darknessTile.col === clickedTile.col && darknessTile.row === clickedTile.row);
                if (!darkTile.alive) {
                    this.clearDarknessTile(clickedTile, true);
                }
            });
        }
    }
};

Board.prototype = Object.create(Phaser.Group.prototype);
Board.prototype.constructor = Board;
Board.prototype.getSurrounding = function (tile) {
    const adjacentTiles = [];
    const relativePositions = [
        { r: 1, c: -1 },
        { r: 1, c: 0 },
        { r: 1, c: 1 },
        { r: 0, c: -1 },
        { r: 0, c: 1 },
        { r: -1, c: -1 },
        { r: -1, c: 0 },
        { r: -1, c: 1 }
    ];
    relativePositions.forEach((relativePosition) => {
        const relativeRow = tile.row + relativePosition.r;
        const relativeColumn = tile.col + relativePosition.c;
        if (
            relativeRow >= 0
            && relativeRow < this.rows
            && relativeColumn >= 0
            && relativeColumn < this.cols
        ) {
            adjacentTiles.push({ row: relativeRow, col: relativeColumn });
        }
    });
    return adjacentTiles;
};
Board.prototype.getXYFromRowCol = function (cell) {
    return {
        x: cell.col * this.tileSize + this.tileSize / 2,
        y: cell.row * this.tileSize + this.tileSize / 2
    };
};
Board.prototype.getFreeCell = function () {
    let freeCell;
    let foundCell = false;
    let row;
    let col;
    const lenght = this.mapElements.length;

    while (!freeCell) {
        foundCell = false;
        row = this.randomBetween(0, this.rows, true);
        col = this.randomBetween(0, this.cols, true);

        for (let i = 0; i < lenght; i += 1) {
            if (
                this.mapElements.children[i].alive
                && this.mapElements.children[i].row === row
                && this.mapElements.children[i].col === col
            ) {
                foundCell = true;
                break;
            }
        }

        if (!foundCell) {
            freeCell = { row, col };
        }
    }
    return freeCell;
};

Board.prototype.randomBetween = function (a, b, isInteger) {
    let numberBetween = a + Math.random() * (b - a);

    if (isInteger) {
        numberBetween = Math.floor(numberBetween);
    }
    return numberBetween;
};

Board.prototype.initItems = function () {
    const numItems = Math.round(
        this.numCells * this.coefs.itemOccupation * this.randomBetween(
            1 - this.coefs.itemVariation,
            1 + this.coefs.itemVariation
        )
    );
    let type;
    let itemData;
    let newItem;
    let cell;
    for (let i = 0; i < numItems; i += 1) {
        type = this.randomBetween(0, this.levelData.itemTypes.length, true);
        itemData = Object.create(this.levelData.itemTypes[type]);
        itemData.board = this;
        itemData.health = itemData.health || 0;
        itemData.attack = itemData.attack || 0;
        itemData.defense = itemData.defense || 0;
        itemData.gold = itemData.gold || 0;
        cell = this.getFreeCell();
        itemData.row = cell.row;
        itemData.col = cell.col;
        newItem = new Item(this.state, itemData);
        this.mapElements.add(newItem);
    }
};

Board.prototype.initEnemies = function () {
    const numEnemies = Math.round(
        this.numCells * this.coefs.enemyOccupation * this.randomBetween(
            1 - this.coefs.enemyVariation,
            1 + this.coefs.enemyVariation
        )
    );
    let type;
    let enemyData;
    let newEnemy;
    let cell;
    for (let i = 0; i < numEnemies; i += 1) {
        type = this.randomBetween(0, this.levelData.enemyTypes.length, true);
        enemyData = Object.create(this.levelData.enemyTypes[type]);
        const coef = this.coefs.levelIncrement ** this.state.currentLevel;
        enemyData.board = this;
        enemyData.health = Math.round(coef * enemyData.health);
        enemyData.attack = Math.round(coef * enemyData.attack);
        enemyData.defense = Math.round(coef * enemyData.defense);
        enemyData.gold = Math.round(coef * enemyData.gold);
        cell = this.getFreeCell();
        enemyData.row = cell.row;
        enemyData.col = cell.col;
        newEnemy = new Enemy(this.state, enemyData);
        this.mapElements.add(newEnemy);
    }
};

Board.prototype.initExit = function () {
    const startCell = this.getFreeCell();
    const start = new Item(this.state, {
        asset: 'start',
        row: startCell.row,
        col: startCell.col,
        type: 'start'
    });
    this.mapElements.add(start);

    const exitCell = this.getFreeCell();
    const exit = new Item(this.state, {
        asset: 'exit',
        row: exitCell.row,
        col: exitCell.col,
        type: 'exit'
    });
    this.mapElements.add(exit);

    const doorKeyCell = this.getFreeCell();
    const doorKey = new Item(this.state, {
        asset: 'keyGreen',
        row: doorKeyCell.row,
        col: doorKeyCell.col,
        type: 'keyGreen'
    });
    this.mapElements.add(doorKey);
    this.clearDarknessTile(start, true);
};

Board.prototype.initDarkness = function () {
    for (let i = 0; i < this.rows; i += 1) {
        for (let j = 0; j < this.cols; j += 1) {
            const tile = new Phaser.Sprite(
                this.game,
                j * this.tileSize,
                i * this.tileSize,
                'darkTile'
            );
            tile.row = i;
            tile.col = j;
            this.darknessTiles.add(tile);
        }
    }
    this.darknessTiles.setAll('alpha', 0.7);
};

Board.prototype.clearDarknessTile = function (tile, considerEnemies) {
    const tiles = this.getSurrounding(tile);
    tiles.push(tile);
    if (considerEnemies) {
        let hasMonster = false;
        for (let i = 0; i < tiles.length; i++) {
            for (let j = 0; j < this.mapElements.length; j++) {
                const enemy = this.mapElements.children[j];
                if (enemy.alive && enemy.data.type === 'enemy' && enemy.visible && enemy.row === tiles[i].row && enemy.col === tiles[i].col) {
                    hasMonster = true;
                    break;
                }
            }
            if (hasMonster) {
                return;
            }
        }
    }
    tiles.forEach((currentTile) => {
        for (let i = 0; i < this.darknessTiles.children.length; i++) {
            const darkTile = this.darknessTiles.children[i];
            if (darkTile.alive && currentTile.row === darkTile.row && currentTile.col === darkTile.col) {
                // search for mapelements
                for (let j = 0; j < this.mapElements.length; j++) {
                    const mapElement = this.mapElements.children[j];
                    if (mapElement.alive && mapElement.row === darkTile.row && mapElement.col === darkTile.col) {
                        mapElement.visible = true;
                        break;
                    }
                }
                darkTile.kill();
                break;
            }
        }
    });
};

Board.prototype.initLevel = function () {
    this.initDarkness();
    this.initItems();
    this.initEnemies();
    this.initExit();
};
export default Board;
