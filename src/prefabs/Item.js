import Phaser from 'phaser';

const Item = function (state, data) {
    const position = state.board.getXYFromRowCol(data);
    Phaser.Sprite.call(this, state.game, position.x, position.y, data.asset);
    this.anchor.setTo(0.5);
    this.game = state.game;
    this.state = state;
    this.board = state.board;
    this.row = data.row;
    this.col = data.col;
    this.data = data;

    this.inputEnabled = true;
    this.visible = false;
    this.events.onInputDown.add(this.collect, this);
};

Item.prototype = Object.create(Phaser.Sprite.prototype);
Item.prototype.constructor = Item;
Item.prototype.collect = function () {
    if (this.data.type === 'consumable') {
        this.state.playerStats.health += this.data.health;
        this.state.playerStats.attack += this.data.attack;
        this.state.playerStats.defense += this.data.defense;
        this.state.playerStats.gold += this.data.gold;
        this.state.refreshStats();
        this.board.clearDarknessTile(this, true);
        this.kill();
    } else if (this.data.type === 'keyGreen') {
        this.state.playerStats.hasKey = true;
        this.state.refreshStats();
        this.board.clearDarknessTile(this, true);
        this.kill();
    } else if (this.data.type === 'exit') {
        this.board.clearDarknessTile(this, true);
        if (this.state.playerStats.hasKey) {
            this.state.playerStats.hasKey = false;
            this.state.nextLevel();
        }
    }
};

export default Item;
