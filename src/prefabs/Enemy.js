import Phaser from 'phaser';

const Enemy = function (state, data) {
    const position = state.board.getXYFromRowCol(data);
    Phaser.Sprite.call(this, state.game, position.x, position.y, data.asset);
    this.anchor.setTo(0.5);
    this.game = state.game;
    this.state = state;
    this.board = state.board;
    this.row = data.row;
    this.col = data.col;
    this.data = data;
    this.data.type = 'enemy';
    this.visible = false;

    const x = 0;
    const y = -4;
    const bitmapRectangle = this.game.add.bitmapData(28, 32);
    bitmapRectangle.ctx.fillStyle = '#0000FF';
    bitmapRectangle.ctx.fillRect(0, 0, 28, 32);
    this.panel = new Phaser.Sprite(this.game, x - 2, y - 2, bitmapRectangle);
    this.panel.alpha = 0.6;
    this.addChild(this.panel);

    const style = {
        font: '7px, Prstart',
        fill: '#fff',
        align: 'left'
    };

    this.healthIcon = new Phaser.Sprite(this.game, x, y, 'heart');
    this.healthIcon.scale.setTo(0.3);
    this.addChild(this.healthIcon);
    this.healthLabel = new Phaser.Text(this.game, x + 10, y, '', style);
    this.addChild(this.healthLabel);

    this.attackIcon = new Phaser.Sprite(this.game, x, y + 10, 'attack');
    this.attackIcon.scale.setTo(0.3);
    this.addChild(this.attackIcon);
    this.attackLabel = new Phaser.Text(this.game, x + 10, y + 10, '', style);
    this.addChild(this.attackLabel);

    this.defenseIcon = new Phaser.Sprite(this.game, x, y + 20, 'defense');
    this.defenseIcon.scale.setTo(0.3);
    this.addChild(this.defenseIcon);
    this.defenseLabel = new Phaser.Text(this.game, x + 10, y + 20, '', style);
    this.addChild(this.defenseLabel);

    this.refreshStats();

    this.inputEnabled = true;
    this.events.onInputDown.add(this.attack, this);
};

Enemy.prototype = Object.create(Phaser.Sprite.prototype);
Enemy.prototype.constructor = Enemy;
Enemy.prototype.refreshStats = function () {
    this.healthLabel.text = Math.ceil(this.data.health);
    this.attackLabel.text = Math.ceil(this.data.attack);
    this.defenseLabel.text = Math.ceil(this.data.defense);
};
Enemy.prototype.attack = function () {
    const attacker = this.state.playerStats;
    const attacked = this.data;
    const damageAttacked = Math.max(
        0.5,
        (attacker.attack * Math.random()) - (attacked.defense * Math.random())
    );
    const damageAttacker = Math.max(
        0.5,
        (attacked.attack * Math.random()) - (attacker.defense * Math.random())
    );
    attacked.health -= damageAttacked;
    attacker.health -= damageAttacker;

    const attackTween = this.game.add.tween(this);
    attackTween.to({ tint: 0xFF0000 }, 300);
    attackTween.onComplete.add(() => {
        this.refreshStats();
        this.tint = 0xFFFFFF;
        if (attacked.health <= 0) {
            attacker.gold += attacked.gold;
            this.kill();
            this.board.clearDarknessTile(this, true);
        }
        if (attacker.health <= 0) {
            this.state.gameOver();
            return;
        }
        this.state.refreshStats();
    });
    attackTween.start();
};

export default Enemy;
