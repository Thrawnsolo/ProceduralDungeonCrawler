/* eslint-disable no-unused-vars */
/* eslint-disable import/extensions */
/* eslint-disable import/no-unresolved */
import PIXI from 'pixi';
import P2 from 'p2';
import Phaser from 'phaser';
import Boot from './states/BootState';
import Preload from './states/PreloadState';
import Game from './states/GameState';

const ProceduralDungeonCrawler = {};
ProceduralDungeonCrawler.game = new Phaser.Game(360, 640, Phaser.AUTO);
ProceduralDungeonCrawler.game.state.add('Boot', Boot());
ProceduralDungeonCrawler.game.state.add('Preload', Preload());
ProceduralDungeonCrawler.game.state.add('Game', Game());
ProceduralDungeonCrawler.game.state.start('Boot');
