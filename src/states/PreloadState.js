import Phaser from 'phaser';

import attack from '../../assets/images/attack.png';
import battleHammer from '../../assets/images/battlehammer.png';
import chestGold from '../../assets/images/chest-gold.png';
import coin from '../../assets/images/coin.png';
import darkTemple from '../../assets/images/dark-temple.png';
import darkTile from '../../assets/images/darkTile.png';
import defense from '../../assets/images/defense.png';
import demon from '../../assets/images/demon.png';
import snake from '../../assets/images/desert-snake.png';
import exit from '../../assets/images/exit.png';
import gold from '../../assets/images/gold.png';
import heart from '../../assets/images/heart.png';
import keyGreen from '../../assets/images/key-green.png';
import leopardWarrior from '../../assets/images/leopard-warrior.png';
import mummy from '../../assets/images/mummyjewel.png';
import orc from '../../assets/images/orc.png';
import potion from '../../assets/images/potion.png';
import profile from '../../assets/images/profile.png';
import questButton from '../../assets/images/quest-button.png';
import rockLand from '../../assets/images/rock-land3.png';
import shield from '../../assets/images/shield.png';
import start from '../../assets/images/start.png';
import sword from '../../assets/images/sword.png';
import skeleton from '../../assets/images/swordskeleton.png';

const imagesAssets = [
    attack,
    battleHammer,
    chestGold,
    coin,
    darkTemple,
    darkTile,
    defense,
    demon,
    snake,
    exit,
    gold,
    heart,
    keyGreen,
    leopardWarrior,
    mummy,
    orc,
    potion,
    profile,
    questButton,
    rockLand,
    shield,
    start,
    sword,
    skeleton
];
const imagesNames = [
    'attack',
    'battleHammer',
    'chestGold',
    'coin',
    'darkTemple',
    'darkTile',
    'defense',
    'demon',
    'snake',
    'exit',
    'gold',
    'heart',
    'keyGreen',
    'leopardWarrior',
    'mummy',
    'orc',
    'potion',
    'profile',
    'questButton',
    'rockLand',
    'shield',
    'start',
    'sword',
    'skeleton'
];

export default () => {
    const obj = new Phaser.State();
    obj.preload = function () {
        this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bar');
        this.preloadBar.anchor.setTo(0.5);
        this.preloadBar.scale.setTo(100, 1);
        this.load.setPreloadSprite(this.preloadBar);

        this.load.images(imagesNames, imagesAssets);
    };
    obj.create = function () {
        this.state.start('Game');
    };
    return obj;
};
