import Phaser from 'phaser';
import Board from '../prefabs/Board';
import gameBaseData from '../../assets/data/gameBaseData.json';
import '../../assets/css/style.css';

const gameData = gameBaseData;
export default () => {
    const gameState = new Phaser.State();
    gameState.init = function (data) {
        this.GAME_CONSTANTS = {
            ROWS: 8,
            COLS: 6,
            TILE_SIZE: 60
        };
        this.data = Object.assign(gameData, data);
        this.currentLevel = this.data.currentLevel || 1;
        this.playerStats = this.data.playerStats || {
            health: 25,
            attack: 2,
            defense: 1,
            gold: 0,
            hasKey: false
        };
    };

    gameState.create = function () {
        this.backgroundTiles = this.add.group();
        this.mapElements = this.add.group();
        this.darknessTiles = this.add.group();
        this.board = new Board(this, {
            rows: this.GAME_CONSTANTS.ROWS,
            cols: this.GAME_CONSTANTS.COLS,
            tileSize: this.GAME_CONSTANTS.TILE_SIZE,
            levelData: this.data
        });

        this.board.initLevel();
        this.initGUI();
    };

    gameState.gameOver = function () {
        this.game.state.start(
            'Game',
            true,
            false,
            {
                currentLevel: 1,
                playerStats: {
                    health: 25,
                    attack: 2,
                    defense: 1,
                    gold: 0,
                    hasKey: false
                }
            }
        );
    };

    gameState.nextLevel = function () {
        this.game.state.start(
            'Game',
            true,
            false,
            { currentLevel: this.currentLevel + 1, playerStats: this.playerStats }
        );
    };

    gameState.initGUI = function () {
        const y = this.GAME_CONSTANTS.TILE_SIZE * this.GAME_CONSTANTS.ROWS;
        const bitmapRect = this.add.bitmapData(this.game.width, this.game.height - y);
        bitmapRect.ctx.fillStyle = '#000058';
        bitmapRect.ctx.fillRect(0, 0, this.game.width, this.game.height - y);

        this.panel = this.add.sprite(0, y, bitmapRect);
        const style = {
            font: '16px Prstart',
            fill: '#fff',
            aligh: 'left'
        };

        this.healthIcon = this.add.sprite(this.game.width - 110, y - 10 + this.GAME_CONSTANTS.TILE_SIZE / 2, 'heart');
        this.healthLabel = this.add.text(this.game.width - 70, y - 10 + this.GAME_CONSTANTS.TILE_SIZE / 2 + 5, '', style);

        this.attackIcon = this.add.sprite(this.game.width - 110, y - 10 + 2 * this.GAME_CONSTANTS.TILE_SIZE / 2, 'attack');
        this.attackLabel = this.add.text(this.game.width - 70, y - 10 + 2 * this.GAME_CONSTANTS.TILE_SIZE / 2 + 5, '', style);

        this.defenseIcon = this.add.sprite(this.game.width - 110, y - 10 + 3 * this.GAME_CONSTANTS.TILE_SIZE / 2, 'defense');
        this.defenseLabel = this.add.text(this.game.width - 70, y - 10 + 3 * this.GAME_CONSTANTS.TILE_SIZE / 2 + 5, '', style);

        this.goldIcon = this.add.sprite(this.game.width - 110, y - 10 + 4 * this.GAME_CONSTANTS.TILE_SIZE / 2, 'gold');
        this.goldLabel = this.add.text(this.game.width - 70, y - 10 + 4 * this.GAME_CONSTANTS.TILE_SIZE / 2 + 5, '', style);

        this.charImage = this.add.sprite(30, y + 16, 'profile');

        const levelStyle = {
            font: '10px Prstart',
            fill: '#fff',
            aligh: 'left'
        };

        this.levelLabel = this.add.text(45, this.game.height - this.GAME_CONSTANTS.TILE_SIZE / 2, '', levelStyle);

        this.keyIcon = this.add.sprite(this.game.width - 180, y - 10 + (this.GAME_CONSTANTS.TILE_SIZE / 2), 'keyGreen');
        this.keyIcon.scale.setTo(0.6);
        this.refreshStats();
    };

    gameState.refreshStats = function () {
        this.healthLabel.text = Math.ceil(this.playerStats.health);
        this.attackLabel.text = Math.ceil(this.playerStats.attack);
        this.defenseLabel.text = Math.ceil(this.playerStats.defense);
        this.goldLabel.text = Math.ceil(this.playerStats.gold);
        this.levelLabel.text = `Floor ${this.currentLevel}`;
        this.keyIcon.visible = this.playerStats.hasKey;
    };

    return gameState;
};
